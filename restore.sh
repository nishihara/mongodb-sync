#!/bin/bash

. /env.sh

MONGODB_HOST=${MONGODB_RESTORE_PORT_27017_TCP_ADDR:-${MONGODB_RESTORE_HOST}}
MONGODB_HOST=${MONGODB_RESTORE_PORT_1_27017_TCP_ADDR:-${MONGODB_RESTORE_HOST}}
MONGODB_PORT=${MONGODB_RESTORE_PORT_27017_TCP_PORT:-${MONGODB_RESTORE_PORT}}
MONGODB_PORT=${MONGODB_RESTORE_PORT_1_27017_TCP_PORT:-${MONGODB_RESTORE_PORT}}
MONGODB_USER=${MONGODB_RESTORE_USER:-${MONGODB_RESTORE_ENV_MONGODB_USER}}
MONGODB_PASS=${MONGODB_RESTORE_PASS:-${MONGODB_RESTORE_ENV_MONGODB_PASS}}

[[ ( -z "${MONGODB_RESTORE_USER}" ) && ( -n "${MONGODB_RESTORE_PASS}" ) ]] && MONGODB_RESTORE_USER='admin'

[[ ( -n "${MONGODB_RESTORE_USER}" ) ]] && USER_RESTORE_STR=" --username ${MONGODB_RESTORE_USER}"
[[ ( -n "${MONGODB_RESTORE_PASS}" ) ]] && PASS_RESTORE_STR=" --password ${MONGODB_RESTORE_PASS}"
[[ ( -n "${MONGODB_RESTORE_DB}" ) ]] && USER_RESTORE_STR=" --db ${MONGODB_RESTORE_DB}"

FILE_TO_RESTORE=(${1})

#[[ -z "${1}" ]] && FILE_TO_RESTORE=$(ls /backup -N1 | grep -iv ".tgz" | sort -r | head -n 1)
FOLDER_TO_RESTORE=$(ls /backup -N1 | grep -P '(?<!\d)\d{4}(?!\d)' | sort -r | head -n 1)
[[ -z "${1}" ]] && FILE_TO_RESTORE=($(ls /backup/$FOLDER_TO_RESTORE))

if [[ -n "$S3_RESTORE" ]]; then

    echo "   Downloading file from S3 before restore..."

    echo "   Downloading archive s3://$S3_BUCKET/$S3_PATH/$S3_RESTORE_BACKUP_NAME.tgz to /backup/toberestored.tgz"
    if [ -z ${ENDPOINT_URL+x} ]; then
        aws s3 cp s3://$S3_BUCKET/$S3_PATH/${S3_RESTORE_BACKUP_NAME}.tgz "/backup/toberestored.tgz"
    else
        aws --endpoint-url=${ENDPOINT_URL} s3 cp s3://$S3_BUCKET/$S3_PATH/${S3_RESTORE_BACKUP_NAME}.tgz "/backup/toberestored.tgz"
    fi

    tar -zxvf "/backup/toberestored.tgz"

    FILE_TO_RESTORE="backup/$S3_RESTORE_BACKUP_NAME/$MONGO_BACKUP_DB"

fi
for i in "${FILE_TO_RESTORE[@]}"
do
    echo "=> Restore database from ${i}"
    if mongorestore --host ${MONGODB_RESTORE_HOST} --port ${MONGODB_RESTORE_PORT} ${USER_RESTORE_STR}${PASS_RESTORE_STR} -d $i --drop /backup/$FOLDER_TO_RESTORE/$i; then
        echo "   Restore succeeded"
    else
        echo "   Restore failed"
    fi
    echo "=> Done"
done
echo "DONE ALL RESTORE"
